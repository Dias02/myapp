package week3.task1.Application;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class  MyApplication  {
    ArrayList<User> users;

    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    public MyApplication(){
        users = new ArrayList<>();
    }

    private void addUser(User user) {
        users.add(user);
    }

    public void start() throws IOException {                //STARTTTTTTTTTTTTTTTTTTTTTT
        readFile();

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else if (choice == 2){
                System.out.println("We are waiting for you again !!!");
                break;
            }
        }

    }

    private void readFile() throws FileNotFoundException {
        File file = new File("C:\\myapp\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while(fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            String[] data = line.split(" ");
            User clone = new User(Integer.parseInt(data[0]),data[1],data[2],data[3], data[4]);
            users.add(clone);
        }
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else return;
            }
            else {
                userProfile();
            }
        }
    }

    private void authentication() throws IOException {
        // sign in
        // sign up
        while(true){
            System.out.println("1. Sign in");
            System.out.println("2. Sign up");
            System.out.println("3. Exit");
            int choice = sc.nextInt();
            if(choice == 1) signIn();
            else if(choice == 2) signUp();
            else if(choice == 3) return;
        }
    }

    private void signIn() throws IOException {
        while (true){
            System.out.println("Enter your username: ");
            String username = sc.next();
            System.out.println("Enter your password:");
            String passwordStr = sc.next();
            if(checkUser(username, passwordStr)){
                signedUser = getUser(username,passwordStr);
                userProfile();
            }
            else{
                System.out.println("MyApplication.signIn: Incorrect username or password!!!");
            }
        }
    }

    private void signUp() throws IOException {
        while(true) {
            System.out.println("Enter your name: ");
            String name = sc.next();
            System.out.println("Enter your surname: ");
            String surname = sc.next();
            System.out.println("Enter your username: ");
            String username = sc.next();
            if (checkUsername(username)) {
                System.out.println("Enter your password: ");
                String passwordStr = sc.next();
                try {
                    signedUser = new User(name, surname, username, passwordStr);
                } catch (Exception i){
                    break;
                }
                break;
            } else {
                System.out.println("This username has already taken!!!");
            }
        }
        addUser(signedUser);
        userProfile();

    }

    private void userProfile() throws IOException {
        while(true) {
            System.out.println(signedUser.getName() + " " + signedUser.getSurname());
            System.out.println("Status: online");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menuList();
            }
            else if (choice == 2) {
                System.out.println("We are waiting for you again !!!");
            }
        }
    }

    public void menuList() throws IOException {
        while(true) {
            System.out.println("1. Friends");
            System.out.println("2. Log off");
            int choice = sc.nextInt();
            if(choice == 1 ){
                friendsMenu();
            }
            else if(choice == 2) {
                logOff();
            }
        }
    }

    private void logOff() throws IOException {
        signedUser = null;
        saveUserList();
        menu();
    }

    public void friendsMenu(){
        while(true){
            System.out.println("1. Friends List");
            System.out.println("2. Add a friend");
            int choice = sc.nextInt();
            if(choice == 1){
                try{ signedUser.friendsList();}
                catch (Exception e) {
                    System.out.println("You don't have friends");
                }
            }
            else if(choice == 2){
                addFriendMenu();
            }
        }
    }

    public void addFriendMenu(){
        while(true){
            System.out.println("Enter name, then surname: ");
            String name = sc.next();
            String surname = sc.next();
            if(checkNewFriend(name,surname)){
                try {
                    User temp = getFriend(name,surname);
                    signedUser.addFriend(temp);
                    System.out.println("Request sent");
                    Files.write(Paths.get("C:\\myapp\\src\\com\\company\\db.txt"), (signedUser.toString() + Objects.requireNonNull(getUser(name, surname)).toString()).getBytes());
                }
                catch (Exception i){
                    System.out.println("User not found!!");
                }
                System.out.println("1. Back");
                int choice = sc.nextInt();
                if(choice == 1) friendsMenu();
            }
        }
    }

    // Checking for existing username
    private boolean checkUsername(String usernameStr){
        for(User user : users){
            if(user.getUsername().equals(usernameStr)){
                return false;
            }
        }
        return true;
    }

    private boolean checkNewFriend(String name, String surname){
        for(User user : users){
            if(user.getName().equals(name) && user.getSurname().equals(surname))
                return true;
        }
        return false;
    }

    // Check for a valid username and password for User
    private boolean checkUser(String username, String password){
        for(User user : users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }

    private User getUser(String username, String password){
        for(User user : users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

    private User getFriend(String name,String surname){
        for(User user : users){
            if(user.getName().equals(name) && user.getSurname().equals(surname)){
                return user;
            }
        }
        return null;
    }


    private void saveUserList() throws IOException {
        Files.write(Paths.get("C:\\myapp\\src\\com\\company\\db.txt"), users.toString().getBytes());
    }
}


